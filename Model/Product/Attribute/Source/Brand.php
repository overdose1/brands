<?php

namespace Overdose\Brands\Model\Product\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Data\OptionSourceInterface;
use Overdose\Brands\Model\BrandRepository;

class Brand extends AbstractSource implements OptionSourceInterface
{
    /**
     * @var BrandRepository
     */
    private $brandRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaInterfaceFactory
     */
    private $searchCriteria;

    /**
     * Construct
     *
     * @param \Overdose\Brands\Model\BrandFactory $brandFactory
     */
    public function __construct(
        BrandRepository $brandRepository,
        \Magento\Framework\Api\SearchCriteriaInterfaceFactory $searchCriteria
    ) {
        $this->brandRepository = $brandRepository;
        $this->searchCriteria = $searchCriteria;
    }

    /*
     * Get list of all available brands
     *
     * @return array
     */
    public function getAllOptions(): array
    {
        /**
         * незнаю правильно ли сделал, возможно запрос нужно делать с модели
         * Но в задании сказано использовать извне только BrandRepository, поэтому так
         */

        // get all brands from table
        $criteria = $this->searchCriteria->create();
        $items = $this->brandRepository->getList($criteria)->getItems();

        // TODO add options caching

        // add empty value (Brand is not required)
        $options[] = [
            'value' => '',
            'label' => ' '
        ];
        foreach ($items as $brand) {
            $options[] = [
                'value' => $brand->getId(),
                'label' => $brand->getBrandName()
            ];
        }

        return $options;
    }
}
