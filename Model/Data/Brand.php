<?php

namespace Overdose\Brands\Model\Data;

use Magento\Framework\Model\AbstractExtensibleModel;
use Overdose\Brands\Api\Data\BrandInterface;

class Brand extends AbstractExtensibleModel implements BrandInterface
{

    protected function _construct()
    {
        $this->_init(\Overdose\Brands\Model\ResourceModel\Brand::class);
    }

    /**
     * Get id
     * @return string|null
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @param mixed|string $id
     * @return Brand
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Get brand_name
     * @return string|null
     */
    public function getBrandName(): string
    {
        return (string)$this->getData(self::BRAND_NAME);
    }

    /**
     * @param string $brandName
     * @return Brand
     */
    public function setBrandName($brandName)
    {
        return $this->setData(self::BRAND_NAME, $brandName);
    }

    /**
     * @return string
     */
    public function getBrandTitle(): string
    {
        return (string)$this->getData(self::BRAND_TITLE);
    }

    /**
     * @param string $brandTitle
     * @return Brand
     */
    public function setBrandTitle(string $brandTitle)
    {
        return $this->setData(self::BRAND_TITLE, $brandTitle);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Magento\Framework\Api\ExtensionAttributesInterface
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Overdose\Brands\Api\Data\BrandExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Overdose\Brands\Api\Data\BrandExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return (string)$this->getData(self::CREATED_AT);
    }

    /**
     * @param string $createdAt
     * @return Brand
     */
    public function setCreatedAt(string $createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return (string)$this->getData(self::UPDATED_AT);
    }

    /**
     * @param string $updatedAt
     * @return Brand
     */
    public function setUpdatedAt(string $updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}

