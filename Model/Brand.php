<?php

namespace Overdose\Brands\Model;

use Magento\Framework\Api\DataObjectHelper;
use Overdose\Brands\Api\Data\BrandInterface;
use Overdose\Brands\Api\Data\BrandInterfaceFactory;

class Brand extends \Magento\Framework\Model\AbstractModel
{

    public const ENTITY = 'overdose_brand';

    protected $brandDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'overdose_brands';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param BrandInterfaceFactory $brandDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Overdose\Brands\Model\ResourceModel\Brand $resource
     * @param \Overdose\Brands\Model\ResourceModel\Brand\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        BrandInterfaceFactory $brandDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Overdose\Brands\Model\ResourceModel\Brand $resource,
        \Overdose\Brands\Model\ResourceModel\Brand\Collection $resourceCollection,
        array $data = []
    ) {
        $this->brandDataFactory = $brandDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve brand model with brand data
     * @return BrandInterface
     */
    public function getDataModel()
    {
        $brandData = $this->getData();

        $brandDataObject = $this->brandDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $brandDataObject,
            $brandData,
            BrandInterface::class
        );

        return $brandDataObject;
    }
}

