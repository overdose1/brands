<?php

namespace Overdose\Brands\Model\ResourceModel\Brand;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Overdose\Brands\Model\Brand::class,
            \Overdose\Brands\Model\ResourceModel\Brand::class
        );
    }
}

