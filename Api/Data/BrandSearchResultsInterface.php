<?php

namespace Overdose\Brands\Api\Data;

interface BrandSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Brand list.
     * @return \Overdose\Brands\Api\Data\BrandInterface[]
     */
    public function getItems();

    /**
     * Set brand_name list.
     * @param \Overdose\Brands\Api\Data\BrandInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

