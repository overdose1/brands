<?php
/**
 * TODO make php7 features
 */
namespace Overdose\Brands\Api\Data;

use Magento\Catalog\Api\Data\ProductInterface;

interface BrandInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    public const ID = 'id';
    public const BRAND_NAME = 'brand_name';
    public const BRAND_TITLE = 'brand_title';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Overdose\Brands\Api\Data\BrandInterface
     */
    public function setId($id);

    /**
     * Get brand_name
     * @return string
     */
    public function getBrandName(): string;

    /**
     * Set brand_name
     * @param string $brandName
     * @return \Overdose\Brands\Api\Data\BrandInterface
     */
    public function setBrandName(string $brandName);

    /**
     * Get brand_name
     * @return string|null
     */
    public function getBrandTitle(): string;

    /**
     * Set brand_title
     * @param string $brandTitle
     * @return \Overdose\Brands\Api\Data\BrandInterface
     */
    public function setBrandTitle(string $brandTitle);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Overdose\Brands\Api\Data\BrandExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Overdose\Brands\Api\Data\BrandExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Overdose\Brands\Api\Data\BrandExtensionInterface $extensionAttributes
    );


    /**
     * Brand created date
     *
     * @return string|null
     */
    public function getCreatedAt(): string;

    /**
     * Set brand created date
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt(string $createdAt);

    /**
     * Brand updated date
     *
     * @return string|null
     */
    public function getUpdatedAt(): string;

    /**
     * Set brand updated date
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt(string $updatedAt);
}

