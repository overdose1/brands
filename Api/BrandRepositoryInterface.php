<?php

namespace Overdose\Brands\Api;

interface BrandRepositoryInterface
{

    /**
     * Save Brand
     * @param \Overdose\Brands\Api\Data\BrandInterface $brand
     * @return \Overdose\Brands\Api\Data\BrandInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Overdose\Brands\Api\Data\BrandInterface $brand
    );

    /**
     * Retrieve Brand
     * @param string $id
     * @return \Overdose\Brands\Api\Data\BrandInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);
    /**
     * Retrieve Brand
     * @param  \Overdose\Brands\Api\Data\BrandInterface $brand
     * @return \Overdose\Brands\Api\Data\BrandInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($brand);

    /**
     * Retrieve Brand matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Overdose\Brands\Api\Data\BrandSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Brand
     * @param \Overdose\Brands\Api\Data\BrandInterface $brand
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Overdose\Brands\Api\Data\BrandInterface $brand
    );

    /**
     * Delete Brand by ID
     * @param string $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}

