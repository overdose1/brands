<?php

/**
 * Plugin for @see \Overdose\Brands\Model\BrandRepository
 *
 * Plugin fills total_brand_products in brand model
 */
namespace Overdose\Brands\Plugin\Model;


use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchResults;
use Overdose\Brands\Api\Data\BrandExtension;
use Overdose\Brands\Api\Data\BrandInterface;
use Overdose\Brands\Model\Data\Brand;

class BrandRepository
{
    /**
     * @var ProductFactory
     */
    private $productRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param \Overdose\Brands\Model\BrandRepository $subject
     * @param SearchResults $result
     */
    public function afterGetList(\Overdose\Brands\Model\BrandRepository $subject, SearchResults $result)
    {
        $items = $result->getItems();
        foreach ($items as $key => $brand) {
            /** @var Brand $brand */
            /** @var BrandExtension $extensionAttributes */
            $extensionAttributes = $brand->getExtensionAttributes();
            $extensionAttributes->setTotalBrandProducts($this->getProductCountByBrand($brand));
        }
        $result->setItems($items);
        return $result;
    }

    public function afterGetById(\Overdose\Brands\Model\BrandRepository $subject, \Overdose\Brands\Api\Data\BrandInterface $result)
    {
        $extensionAttributes = $result->getExtensionAttributes();
        $extensionAttributes->setTotalBrandProducts($this->getProductCountByBrand($result));

        return $result;
    }

    public function afterGet(\Overdose\Brands\Model\BrandRepository $subject, \Overdose\Brands\Api\Data\BrandInterface $result)
    {
        $extensionAttributes = $result->getExtensionAttributes();
        $extensionAttributes->setTotalBrandProducts($this->getProductCountByBrand($result));

        return $result;
    }


    /**
     * Get total product count by brand
     *
     * @param Brand $brand
     * @return int
     */
    private function getProductCountByBrand(BrandInterface $brand): int
    {
            // TODO make sense to add caching for query result
            $this->searchCriteriaBuilder
                ->addFilter('brand', $brand->getId());

            $searchCriteria = $this->searchCriteriaBuilder->create();

            $products = $this->productRepository
                ->getList($searchCriteria);


        return $products->getTotalCount();
    }

}
