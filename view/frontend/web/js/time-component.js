define([
    "jquery"
], function($) {
    "use strict";

    return function (config, element) {
        var deferred = $.Deferred();
        var ajaxUrl = config.AjaxUrl;

        $.ajax({
            url: ajaxUrl,
            type: "POST",
            dataType: 'json',
            success: function (responce) {
                $('.time-wrap').show(0);
                $('.time-wrap .time-block').html(responce.time);
                deferred.resolve();
            },
            error: function (response) {
                var message;

                if (typeof response.responseJSON === 'undefined' ||
                    typeof response.responseJSON.message === 'undefined'
                ) {
                    message = 'Could not save the asset!';
                } else {
                    message = response.responseJSON.message;
                }
                deferred.reject(message);
            }
        });

        return deferred.promise();
    };
});
