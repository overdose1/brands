<?php


namespace Overdose\Brands\Block;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;


/**
 * Solution took here https://magento.stackexchange.com/questions/280681/modify-product-title-display
 */
class ProductTitle extends \Magento\Theme\Block\Html\Title
{
    /**
     * @var \Magento\Framework\Registry
     */
    private  $registry;

    /**
     * @var \Overdose\Brands\Api\BrandRepositoryInterface
     */
    private $brandRepository;

    public function __construct(
        \Overdose\Brands\Api\BrandRepositoryInterface $brandRepository,
        \Magento\Framework\Registry $registry,
        Template\Context $context,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {
        $this->registry = $registry;
        $this->brandRepository = $brandRepository;
        parent::__construct($context, $scopeConfig, $data);
    }

    public function setPageTitle($pageTitle)
    {
        parent::setPageTitle($pageTitle);

        $product = $this->getCurrentProduct();
        if ($product) {
            $brandId = $product->getData('brand');
            if (is_string($brandId)) {
                $brandModel = $this->brandRepository->getById($brandId);
                if ($brandModel) {
                    // TODO translate str
                    $this->pageTitle .= ' (' . $brandModel->getBrandName() . ', ' . $brandModel->getExtensionAttributes()->getTotalBrandProducts() . ' total)';
                }
            }
        }



    }

    /**
     * Returns product if exist
     *
     * @return false|Product
     */
    public function getCurrentProduct()
    {
        //looks like this was is deprecated, but I not fould other variant for product detecting
        if ($this->registry->registry('current_product')) {
            return $this->registry->registry('current_product');
        }
        return false;
    }
}
