<?php


namespace Overdose\Brands\Block\Product;


use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Helper\Output as OutputHelper;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Url\Helper\Data;
use Overdose\Brands\Api\BrandRepositoryInterface;
use Overdose\Brands\Model\BrandRepository;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    /**
     * Flag
     *
     * @var bool
     */
    private $brandAdded = false;

    /**
     * @var BrandRepository
     */
    private $brandRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(
        BrandRepositoryInterface $brandRepository,
        ProductRepository $productRepository,
        Context $context,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        array $data = [],
        ?OutputHelper $outputHelper = null
    ) {
        $this->brandRepository = $brandRepository;
        $this->productRepository = $productRepository;

        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data,
            $outputHelper
        );
    }

    public function getLoadedProductCollection()
    {
        $parentCollection = parent::getLoadedProductCollection();

        if (!$this->brandAdded) {
            $products = $parentCollection->getItems();
            foreach ($parentCollection->getItems() as $product) {
                /** @var Product $product */
                $product->setName($this->getProductNameWithBrandInfo($product));
            }

            $this->brandAdded = true;
        }

        return $parentCollection;
    }

    private function getProductNameWithBrandInfo(Product $product): string
    {
        /**
         * в $product небыло brand-a, хотя в файле catalog_attributes.xml все указал.
         * Проблему не нашел.
         * Использую товар с репозитория
         */
        $productFromRepo = $this->productRepository->getById($product->getId());
        $brandId = $productFromRepo->getData('brand');

        if (is_string($brandId)) {
            $brandModel = $this->brandRepository->getById($brandId);
            if ($brandModel) {
                // TODO translate str
                // not $product->getName because plugin recursion
                return $product->getName() . ' (' . $brandModel->getBrandName() . ', ' . $brandModel->getExtensionAttributes()->getTotalBrandProducts() . ' total)';
            }
        }

        return $product->getName();
    }

}
