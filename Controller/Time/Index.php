<?php

namespace Overdose\Brands\Controller\Time;

/**
 * Ajax example took here https://magento.stackexchange.com/questions/128669/how-to-make-a-simple-ajax-call-in-magento-2-1-0
 */
use \Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Index extends Action
{

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {

            $data = [
                'time' => date('Y-m-d H:i:s')
            ];

            return $result->setData($data);
        }
    }
}
